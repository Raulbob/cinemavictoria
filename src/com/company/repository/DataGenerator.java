package com.company.repository;

import com.company.model.*;

import java.util.Arrays;

public class DataGenerator {

    public static Cinema buildCinema() {
        //CREATE CLIENTS
        Client client = new Client("Raul Bob", "raul@gmail.com", "187382178321");
        Client client1 = new Client("Marcel", "marc@gmail.com", "09763217312");
        Client client2 = new Client("Flow", "flo@gmail.com", "0733232133213");
        Client client3 = new Client("Emilia", "emi@gmail.com", "0738493933");

        //CRATE MOVIES
        Movie peterPanMovie = new Movie("Peter Pan", "Clark Kent");
        Movie harryPotterMovie = new Movie("HarryPotter", "Maria Fernandez");
        Movie supermanMovie = new Movie("Superman", "Elena Mark");

        //CREATE EVENTS
        Event peterPanMovieEvent = new MovieEvent(peterPanMovie, "03/17/20222:16:00", "room1");
        Event supermanMovieEvent = new MovieEvent(supermanMovie, "03/18/20222:20:00", "room2");
        Event harryPotterMovieEvent = new MovieEvent(harryPotterMovie, "03/20/20222:20:00", "room2");
        Event extraEvent = new EventExtra("03/17/20222:22:00", "room1", "Cinema Victoria HW", "Raul Bob"); //(String dateHour, String room, String name, String personOfContact)

        //CREATE RESERVATIONS
        extraEvent.getReservations().addAll(Arrays.asList(client, client1, client2, client3));

        //CREATE CINEMA
        Cinema cinema = new Cinema("Cinema Victoria", Arrays.asList(peterPanMovieEvent, supermanMovieEvent, extraEvent, harryPotterMovieEvent));
        return cinema;
    }


}
