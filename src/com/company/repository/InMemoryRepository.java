package com.company.repository;

import com.company.model.*;

import java.util.Arrays;
import java.util.List;

/**
 * or DAO. Your choice.
 */
public class InMemoryRepository {

    private Cinema cinema;

    public InMemoryRepository() {
        cinema = DataGenerator.buildCinema();
    }

    public List<Event> getCalendar() {
        return cinema.getCalendar();
    }

    public void ticketReservation(Event chosenEvent, Client client) {
        chosenEvent.getReservations().add(client);
    }

}
