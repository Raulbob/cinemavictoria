package com.company.model;

import java.util.ArrayList;
import java.util.List;

public class Cinema {

    private String name;
    private List<Event> calendar;

    public Cinema(String name, List<Event> calendar) {
        this.name = name;
        this.calendar = calendar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Event> getCalendar() {
        if (calendar == null) {
            calendar = new ArrayList<>();
        }
        return calendar;
    }

}
