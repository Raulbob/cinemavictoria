package com.company.model;

public class EventExtra extends Event {

    private String personOfContact;

    public EventExtra(String dateHour, String room, String name, String personOfContact) {
        super(name, dateHour, room);
        this.personOfContact = personOfContact;
    }

    public String getPersonOfContact() {
        return personOfContact;
    }

    public void setPersonOfContact(String personOfContact) {
        this.personOfContact = personOfContact;
    }

}
