package com.company.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Event {

    private String name;
    private String dateHour;
    private String room;
    private List<Client> reservations;

    public Event(String name, String dateHour, String room) {
        this.name = name;
        this.dateHour = dateHour;
        this.room = room;
    }

    public String getDateHour() {
        return dateHour;
    }

    public void setDateHour(String dateHour) {
        this.dateHour = dateHour;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public List<Client> getReservations() {
        if (reservations == null) {
            reservations = new ArrayList<>();
        }
        return reservations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + ", " + dateHour + ", " + room + ", reservations:" + reservations;
    }

}
