package com.company.model;

public class MovieEvent extends Event {

    private Movie movie;

    public MovieEvent(Movie movie, String dateHour, String room) {
        super(movie.getName(), dateHour, room);
        this.movie = movie;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

}
