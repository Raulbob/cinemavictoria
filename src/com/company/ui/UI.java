package com.company.ui;

import com.company.model.Event;
import com.company.service.Service;

import java.util.List;

import static com.company.ui.Keyboard.*;

public class UI {

    private static final String GENERIC_ERROR_MESSAGE = "!!! Error. Invalid Input. Try again !!!\n";

    private final Service service;

    public UI(Service service) {
        this.service = service;
    }

    public void run() {
        while (true) {
            System.out.println(buildMenu());
            System.out.println("Option:");
            String option = readString();

            try {
                switch (option) {
                    case "1":
                        ticketReservation();
                        break;
                    case "2":
                        addMovieToCalendar();
                        break;
                    case "3":
                        addExtraEventToCalendar();
                        break;
                    case "0":
                        return;
                }
            } catch (Exception exception) {
                System.out.println(GENERIC_ERROR_MESSAGE);
            }
        }
    }

    /**
     * Reserve a ticket by the client.
     */
    private void ticketReservation() throws UIException {
        System.out.println("Alege eventimentul la care vrei sa rezervi bilet:");

        List<Event> events = service.getAllEvents();
        System.out.println(prettyPrint(events));
        System.out.println("Option:");
        String option = readString();

        int index = Integer.parseInt(option) - 1;
        if (index >= events.size() || index < 0) {
            throw new UIException();
        }

        Event chosenEvent = events.get(index);
        System.out.println("Please enter details of the person to whom you're making a reservation!");

        System.out.print("Name:");
        String name = readString();

        System.out.print("Email:");
        String email = readString();

        System.out.print("PhoneNo:");
        String phoneNo = readString();
        System.out.println("");

        service.ticketReservation(name, email, phoneNo, chosenEvent);
    }


    private String prettyPrint(List<Event> allEventsInTheFuture) {
        String string = "";
        for (int i = 0; i < allEventsInTheFuture.size(); i++) {
            Event event = allEventsInTheFuture.get(i);
            string += (i + 1) + ". " + event.toString() + "\n";
        }
        return string;
    }


    private void addExtraEventToCalendar() {
        System.out.println("addExtraEventToCalendar");
        System.out.println("Not Implemented!!!");
    }

    private void addMovieToCalendar() {
        System.out.println("addMovieToCalendar");
        System.out.println("Not Implemented!!!");
    }


    private String buildMenu() {
        return "~~ Welcome to Cinema Victoria!!! ~~\n"
                + "1. Rezervare bilete la film de către clienți\n"
                + "2. Adăugare Film în calendarul cinematografului\n"
                + "3. Adaugare eveniment extra în calendar\n"
                + "0. Exit";
    }


}
