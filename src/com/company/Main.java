package com.company;

import com.company.model.*;
import com.company.repository.InMemoryRepository;
import com.company.service.Service;
import com.company.ui.UI;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        InMemoryRepository repository = new InMemoryRepository();
        Service service = new Service(repository);
        UI ui = new UI(service);
        ui.run();
    }

}
