package com.company.service;

import com.company.model.Client;
import com.company.model.Event;
import com.company.repository.InMemoryRepository;

import java.util.List;

public class Service {

    private InMemoryRepository repository;

    public Service(InMemoryRepository repository) {
        this.repository = repository;
    }

    public List<Event> getAllEvents() {
        return repository.getCalendar();
    }

    public void ticketReservation(String name, String email, String phoneNo, Event chosenEvent) {
        Client client = new Client(name, email, phoneNo);
        repository.ticketReservation(chosenEvent, client);
    }

}
